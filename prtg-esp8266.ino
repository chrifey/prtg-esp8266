/*
 *  Very simple sketch to set the color of a  ws2812b
 *  LED via http GET commands. The idea is to set the colors
 *  from PRTG in order to display status infos.
 */

#include <ESP8266WiFi.h>
#include <Adafruit_NeoPixel.h>

// Set pin for led strip
#define PIN 5
Adafruit_NeoPixel strip = Adafruit_NeoPixel(1, PIN, NEO_GRB + NEO_KHZ800);

const char* ssid = "XXXXXX";
const char* password = "XXXXXX";

// Create an instance of the server
// specify the port to listen on as an argument
WiFiServer server(80);

void setup() {
  Serial.begin(115200);
  delay(10);

  strip.begin();
  strip.show(); // Initialize all pixels to 'off'

  // Connect to WiFi network
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    strip.setPixelColor(0, 0, 0, 255); strip.show();delay(500);strip.setPixelColor(0, 0, 0, 0); strip.show();delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  strip.setPixelColor(0, 0, 0, 0); strip.show();
  
  // Start the server
  server.begin();
  Serial.println("Server started");

  // Print the IP address
  Serial.println(WiFi.localIP());
}

void loop() {
   //connect wifi if not connected
   if (WiFi.status() != WL_CONNECTED) {
    delay(1);
    setup();
    return;
  }

  // Check if a client has connected
  WiFiClient client = server.available();
  if (!client) {
    return;
  }
  
  // Wait until the client sends some data
  Serial.println("new client");
  while(!client.available()){
    delay(1);
  }
  
  // Read the first line of the request
  String req = client.readStringUntil('\r');
  Serial.println(req);
  client.flush();
  
  // Match the request
  if (req.indexOf("/color/blue") != -1) { strip.setPixelColor(0, 0, 0, 255); strip.show(); }
  else if (req.indexOf("/color/red") != -1) {strip.setPixelColor(0, 255, 0, 0); strip.show();}
  else if (req.indexOf("/color/yellow") != -1) {strip.setPixelColor(0, 255, 255, 0); strip.show();}
  else if (req.indexOf("/color/green") != -1) {strip.setPixelColor(0, 0, 255, 0); strip.show();}
  else if (req.indexOf("/color/white") != -1) {strip.setPixelColor(0, 255, 255, 255); strip.show();}
  else if (req.indexOf("/color/black") != -1) {strip.setPixelColor(0, 0, 0, 0); strip.show();}
  else if (req.indexOf("/blink/red") != -1) { for(int i=0; i< 10; i++) { strip.setPixelColor(0, 255, 0, 0); strip.show();delay(1000);strip.setPixelColor(0, 0, 0, 0); strip.show();delay(1000);} }
  else if (req.indexOf("/blink/blue") != -1) { for(int i=0; i< 10; i++) { strip.setPixelColor(0, 0, 0, 255); strip.show();delay(1000);strip.setPixelColor(0, 0, 0, 0); strip.show();delay(1000);} }
  else if (req.indexOf("/blink/green") != -1) { for(int i=0; i< 10; i++) { strip.setPixelColor(0, 0, 255, 0); strip.show();delay(1000);strip.setPixelColor(0, 0, 0, 0); strip.show();delay(1000);} }
  else if (req.indexOf("/blink/yellow") != -1) { for(int i=0; i< 10; i++) { strip.setPixelColor(0, 255, 255, 0); strip.show();delay(1000);strip.setPixelColor(0, 0, 0, 0); strip.show();delay(1000);} }
  else if (req.indexOf("/blink/white") != -1) { for(int i=0; i< 10; i++) { strip.setPixelColor(0, 255, 255, 255); strip.show();delay(1000);strip.setPixelColor(0, 0, 0, 0); strip.show();delay(1000);} }
  else if (req.indexOf("/misc/chase1") != -1) { rainbow(20); } // red
  else if (req.indexOf("/misc/chase2") != -1) { rainbowCycle(20); } // green

    
  else {
    Serial.println("invalid request");
    client.stop();
    return;
  }

  // Prepare the response
  String s = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n<!DOCTYPE HTML>\r\n<html>\r\nRequst accepted ";
  s += "</html>\n";

  // Send the response to the client
  client.print(s);
  delay(1);
  Serial.println("Client disonnected");

  // The client will actually be disconnected 
  // when the function returns and 'client' object is detroyed
}

void rainbow(uint8_t wait) {
  uint16_t i, j;

  for(j=0; j<256; j++) {
    for(i=0; i<strip.numPixels(); i++) {
      strip.setPixelColor(i, Wheel((i+j) & 255));
    }
    strip.show();
    delay(wait);
  }
}

// Slightly different, this makes the rainbow equally distributed throughout
void rainbowCycle(uint8_t wait) {
  uint16_t i, j;

  for(j=0; j<256*5; j++) { // 5 cycles of all colors on wheel
    for(i=0; i< strip.numPixels(); i++) {
      strip.setPixelColor(i, Wheel(((i * 256 / strip.numPixels()) + j) & 255));
    }
    strip.show();
    delay(wait);
  }
}

// Input a value 0 to 255 to get a color value.
// The colours are a transition r - g - b - back to r.
uint32_t Wheel(byte WheelPos) {
  WheelPos = 255 - WheelPos;
  if(WheelPos < 85) {
    return strip.Color(255 - WheelPos * 3, 0, WheelPos * 3);
  }
  if(WheelPos < 170) {
    WheelPos -= 85;
    return strip.Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
  WheelPos -= 170;
  return strip.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
}

