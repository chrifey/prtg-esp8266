## Overview

This project aims to create a little IoT LED Cube that can be controlled via http.
With that, it should be possible the display PRTG Status information.

You need the Adafruit NeoPixel Pixel and the esp8266 library in order to compile the sketch.

## Hardware

nodemcu board (esp8266) and ws2812 led pixel.

## Prereqs

Add your ESP8266 board to Arduino IDE with board manager like explained here:
https://github.com/esp8266/Arduino

## Usage

Set your SSID and WiFi password and flash the sketch to your device.
After powering on it will blink blue until WiFi has connected successfully.

The following features are implemented:

### setting the color
http://1.2.3.4/color/[blue red green red white black]

### led the led blink
http://1.2.3.4/blink/[blue red green red white black]

### color chase
http://1.2.3.4/misc/[chase1 chase2]


